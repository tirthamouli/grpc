package main

import (
	"fmt"
	"log"

	"github.com/golang/protobuf/proto"
)

func main() {
	// Step 1: Create data
	tirth := &Person{
		Name: "Tirth",
		Age:  23,
		SocialFollowers: &SocialFollowers{
			Twitter: 0,
			Youtube: 1000,
		},
	}

	// Step 2: Marshall data
	data, err := proto.Marshal(tirth)

	// Step 3: Error Handling and printing data
	if err != nil {
		log.Fatal("Marshalling error")
	}
	fmt.Println("Marshalled data:", data)

	// Step 4: Unmarshall Data
	newTirth := &Person{}
	err = proto.Unmarshal(data, newTirth)
	if err != nil {
		log.Fatal("Un-marshalling error")
	}
	fmt.Println("Name:", newTirth.GetName())
	fmt.Println("Age:", newTirth.GetAge())
	fmt.Println("Twitter followers:", newTirth.GetSocialFollowers().GetTwitter())
	fmt.Println("Youtube followers:", newTirth.GetSocialFollowers().GetYoutube())

}
