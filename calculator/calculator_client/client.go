package main

import (
	"context"
	"fmt"
	"io"
	"log"

	"github.com/tirthamoulib/grpc/calculator/calculatorpb"
	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client created")

	// Step 3: Connect to grpc server
	cc, err := grpc.Dial("localhost:6000", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer cc.Close()

	// Step 4: Create a client from the connection
	c := calculatorpb.NewCalculatorServiceClient(cc)

	// Step 5: Calculate the sum/prime decom/avg/max
	max(c)
}

func sum(c calculatorpb.CalculatorServiceClient) {
	// Step 1: Get the two numbers from the user
	var num1, num2 int64
	fmt.Print("Enter the first number: ")
	fmt.Scanf("%d", &num1)
	fmt.Print("Enter the second number: ")
	fmt.Scanf("%d", &num2)

	// Step 1: Make remote procedure call
	res, err := c.Sum(context.Background(), &calculatorpb.SumRequest{
		Num1: num1,
		Num2: num2,
	})
	if err != nil {
		log.Fatal(err)
	}

	// Step 2: Print the result
	fmt.Println("Sum is:", res.GetResult())
}

func primeNumberDecomposition(c calculatorpb.CalculatorServiceClient) {
	// Step 1: Get the numbers from the user
	var num int64
	fmt.Print("Enter the number: ")
	fmt.Scanf("%d", &num)

	// Step 1: Make remote procedure call
	res, err := c.PrimeNumberDecomposition(context.Background(), &calculatorpb.PrimeNumberDecompositionRequest{
		Num: num,
	})
	if err != nil {
		log.Fatal("Error while getting response", err)
	}

	// Step 2: Loop through and print the result
	for {
		result, err := res.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal("Error while steaming", err)
		}
		fmt.Println(result.GetResult())
	}
}

func average(c calculatorpb.CalculatorServiceClient) {
	var n, temp int64

	// Step 1: Get the total number of elements
	fmt.Print("Enter the number of elements you want to enter: ")
	fmt.Scanf("%d", &n)

	// Step 2: Get the stream
	stream, err := c.Average(context.Background())
	if err != nil {
		log.Fatal("Error while calling average", err)
	}

	// Step 3: Loop and get the numbers and send to the server
	for i := int64(0); i < n; i++ {
		fmt.Print("Enter number: ")
		fmt.Scanf("%d", &temp)
		stream.Send(&calculatorpb.AverageRequest{
			Num: temp,
		})
	}

	// Step 4: Get the result
	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatal("Error with getting result:", err)
	}

	// Step 5: Print the result
	fmt.Println("Average:", res.GetResult())
}

func max(c calculatorpb.CalculatorServiceClient) {
	var n, temp int64

	// Step 1: Get the total number of elements
	fmt.Print("Enter the number of elements you want to enter: ")
	fmt.Scanf("%d", &n)

	// Step 2: Get the stream
	stream, err := c.Max(context.Background())
	if err != nil {
		log.Fatal("Error while calling average", err)
	}

	// Step 3: Wait for response in another routine
	waitc := make(chan struct{})
	go func() {
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				break
			} else if err != nil {
				log.Fatal("Error while receiving stream:", err)
			}
			fmt.Println("Current max is:", res.GetMax())
		}
		close(waitc)
	}()

	// Step 4: Loop and get the numbers and send to the server
	for i := int64(0); i < n; i++ {
		fmt.Println("Enter number:")
		fmt.Scanf("%d", &temp)
		stream.Send(&calculatorpb.MaxRequest{
			Num: temp,
		})
	}

	// Step 5: Close the send stream
	stream.CloseSend()

	// Step 6: Wait until all messages have been received
	<-waitc
}
