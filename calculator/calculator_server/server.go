package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"github.com/tirthamoulib/grpc/calculator/calculatorpb"
	"google.golang.org/grpc"
)

type server struct{}

func (*server) Sum(ctx context.Context, in *calculatorpb.SumRequest) (*calculatorpb.SumResponse, error) {
	fmt.Println("Sum called with", in)

	// Step 1: Get the two numbers
	num1 := in.GetNum1()
	num2 := in.GetNum2()

	// Step 2: Add the numbers and create response
	res := &calculatorpb.SumResponse{
		Result: (num1 + num2),
	}

	// Step 3: Return the response
	return res, nil
}

func (*server) PrimeNumberDecomposition(in *calculatorpb.PrimeNumberDecompositionRequest, stream calculatorpb.CalculatorService_PrimeNumberDecompositionServer) error {
	fmt.Println("Prime number decomposition called with", in)

	// Step 1: Get the num and inititate the smallest prime number
	num := in.GetNum()
	i := int64(2)

	// Step 2: Loop through and send the response
	for num > 1 {
		if num%i == 0 {
			num = num / i
			stream.Send(&calculatorpb.PrimeNumberDecompositionResponse{
				Result: i,
			})
		} else {
			i++
		}
		time.Sleep(1000 * time.Millisecond)
	}

	// Step 3: Return nil
	return nil
}

func (*server) Average(stream calculatorpb.CalculatorService_AverageServer) error {
	fmt.Println("Average called")

	// Step 1: Default sum
	sum := 0
	totalNum := 0

	// Step 2: Loop through the numbers and calculate the average
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			if totalNum == 0 {
				return stream.SendAndClose(&calculatorpb.AverageResponse{
					Result: 0,
				})
			}
			return stream.SendAndClose(&calculatorpb.AverageResponse{
				Result: float64(sum) / float64(totalNum),
			})

		} else if err != nil {
			log.Fatal("Error while streaming:", err)
		}
		fmt.Println(req.GetNum())
		sum += int(req.GetNum())
		totalNum++
	}
}

func (*server) Max(stream calculatorpb.CalculatorService_MaxServer) error {
	// Step 1: Default max value
	max := int64(0)
	flag := false

	// Step 2: Looping through the inputs, getting the numbers and sending back the current max
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		} else if err != nil {
			log.Fatal("Error while receiving request:", err)
		}
		num := req.GetNum()
		if num > max || !flag {
			max = num
			flag = true
		}
		stream.Send(&calculatorpb.MaxResponse{
			Max: max,
		})
	}
}

func main() {
	fmt.Println("Starting calculator server in port 6000")

	// Step 1: Listen to port 6000
	lis, err := net.Listen("tcp", "0.0.0.0:6000")
	if err != nil {
		log.Fatal("Failed while listening", err)
	}

	// Step 2: Create a grpc server
	s := grpc.NewServer()

	// Step 3: Resgister the service
	calculatorpb.RegisterCalculatorServiceServer(s, &server{})

	// Step 4: Use the listener for the server
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
