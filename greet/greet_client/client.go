package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/tirthamoulib/grpc/greet/greetpb"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client calling server")

	// Step 1: Connecting to grpc server
	cc, err := grpc.Dial("localhost:6000", grpc.WithInsecure()) // grpc.WithInsecure since we don't have SSL certificates
	if err != nil {
		log.Fatal(err)
	}
	// Step 2: Defer close the connection
	defer cc.Close()

	// Step 3: Create a client from the connection
	c := greetpb.NewGreetServiceClient(cc)

	// Step 4: Make unary request
	// doUnary(c)

	// Step 5: Make server streaming request
	// doServerStreaming(c)

	// Step 6: Make client streaming request
	doBidirectionalStreaming(c)
}

func doUnary(c greetpb.GreetServiceClient) {
	// Step 1: Create greet request
	req := &greetpb.GreetRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Monkey",
			LastName:  "Luffy",
		},
	}

	// Step 2: Make the rpc
	res, err := c.Greet(context.Background(), req)
	if err != nil {
		log.Fatal(err)
	}

	// Step 3: Print the response
	fmt.Println(res.GetResult())
}

func doServerStreaming(c greetpb.GreetServiceClient) {
	// Step 1: Create greet request
	req := &greetpb.GreetManyTimesRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Monkey",
			LastName:  "Luffy",
		},
	}

	// Step 2: Make the rpc
	res, err := c.GreetManyTimes(context.Background(), req)
	if err != nil {
		log.Fatal(err)
	}

	// Step 3: Loop through and print all the result
	for {
		result, err := res.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal("Error while receiving streaming")
		}
		fmt.Println(result.GetResult())
	}
}

func doClientStreaming(c greetpb.GreetServiceClient) {
	// Step 1: Create the name array
	names := [][]string{
		[]string{"Monkey", "Luffy"},
		[]string{"Tirth", "Baidya"},
		[]string{"Roronoa", "Zoro"},
		[]string{"Tony", "Chopper"},
		[]string{"Sanji", "Vinsmoke"},
		[]string{"Nico", "Robin"},
		[]string{"Sawada", "Tsunayoshi"},
	}

	// Step 2: Get the stream
	stream, err := c.LongGreet(context.Background())
	if err != nil {
		log.Fatal("Error while calling long greet", err)
	}

	// Step 3: Loop through and send the name
	for _, name := range names {
		req := &greetpb.LongGreetRequest{
			Greeting: &greetpb.Greeting{
				FirstName: name[0],
				LastName:  name[1],
			},
		}
		stream.Send(req)
		time.Sleep(1000 * time.Millisecond)
	}

	result, err := stream.CloseAndRecv()

	if err != nil {
		log.Fatal("Error while receiving result")
	}

	fmt.Println(result.GetResult())
}

func doBidirectionalStreaming(c greetpb.GreetServiceClient) {
	// Step 1: Create the name array
	names := [][]string{
		[]string{"Monkey", "Luffy"},
		[]string{"Tirth", "Baidya"},
		[]string{"Roronoa", "Zoro"},
		[]string{"Tony", "Chopper"},
		[]string{"Sanji", "Vinsmoke"},
		[]string{"Nico", "Robin"},
		[]string{"Sawada", "Tsunayoshi"},
	}

	// Step 2: Get the stream
	stream, err := c.GreetEveryone(context.Background())
	if err != nil {
		log.Fatal("Error while calling long greet", err)
	}

	// Step 3: Listen to for receive in another go routine
	ch := make(chan bool)
	go greetEveryoneListen(stream, ch)

	// Step 4: Send to the send stream
	for _, name := range names {
		req := &greetpb.GreetEveryoneRequest{
			Greeting: &greetpb.Greeting{
				FirstName: name[0],
				LastName:  name[1],
			},
		}
		stream.Send(req)
		time.Sleep(1000 * time.Millisecond)
	}

	// Step 5: Close the send stream
	stream.CloseSend()

	// Step 6: Wait for receive to close
	fmt.Println("Waiting for receive to end")
	fmt.Println("Receive ended with", <-ch)
}

func greetEveryoneListen(stream greetpb.GreetService_GreetEveryoneClient, ch chan<- bool) {
	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal("Error receiving stream:", err)
		}
		fmt.Println(res.GetResult())
	}

	ch <- true
}
