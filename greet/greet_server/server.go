package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"time"

	"github.com/tirthamoulib/grpc/greet/greetpb"
	"google.golang.org/grpc"
)

type server struct{}

func (*server) Greet(ctx context.Context, in *greetpb.GreetRequest) (*greetpb.GreetResponse, error) {
	fmt.Println("Greet function was called with", in)

	// Step 1: Get the first name from the input
	firstName := in.GetGreeting().GetFirstName()

	// Step 2: Form the result
	result := "Hello " + firstName
	res := &greetpb.GreetResponse{
		Result: result,
	}

	// Step 3: Return the response
	return res, nil
}

func handleGreetManyTimes(firstName string, stream greetpb.GreetService_GreetManyTimesServer, ch chan<- bool) {
	// Step 1: Run a loop for 10 times
	for i := 0; i < 10; i++ {
		// Step 2: Generate response
		result := "Hello " + firstName + " number " + strconv.Itoa(i)
		res := &greetpb.GreetManytimesResponse{
			Result: result,
		}

		// Step 3: Send the response
		stream.Send(res)

		// Step 4: Sleep for 1000 miliseconds
		time.Sleep(1000 * time.Millisecond)
	}

	// Step 5: Send true to signal that all work is done
	ch <- (true)
}

func (*server) GreetManyTimes(in *greetpb.GreetManyTimesRequest, stream greetpb.GreetService_GreetManyTimesServer) error {
	fmt.Println("Greet many times function was called with", in)

	// Step 1: Get the first name
	firstName := in.GetGreeting().GetFirstName()

	// Step 2: Handle the response in another goroutine
	ch := make(chan bool)
	go handleGreetManyTimes(firstName, stream, ch)

	// Wait for channel to return
	fmt.Println("Sent from channel", <-ch)

	// Step 3: Return nil
	return nil
}

func (*server) LongGreet(stream greetpb.GreetService_LongGreetServer) error {
	fmt.Println("Long greet function was called with", stream)

	// Step 1: Initialize result
	result := ""

	// Step 2: Loop and receive the response
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return stream.SendAndClose(&greetpb.LongGreetResponse{
				Result: result,
			})
		} else if err != nil {
			log.Fatal("Error while streaming", err)
		}
		fmt.Println(req.GetGreeting().GetFirstName(), req.GetGreeting().GetLastName())
		result += "Hello " + req.GetGreeting().GetFirstName() + "! "
	}
}

func (*server) GreetEveryone(stream greetpb.GreetService_GreetEveryoneServer) error {
	fmt.Println("Greet everyone function was called with", stream)

	// Step 1: Loop through the request and respond to all of them
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal("Error while streaming", err)
		}
		fmt.Println(req.GetGreeting().GetFirstName(), req.GetGreeting().GetLastName())
		stream.Send(&greetpb.GreetEveryoneResponse{
			Result: "Hello " + req.GetGreeting().GetFirstName() + "!",
		})
	}

	// Step 2: Return nil error
	return nil
}

func main() {
	fmt.Println("Starting server in port 6000")

	// Step 1: Create listener: using net package to listen to a port
	lis, err := net.Listen("tcp", "0.0.0.0:6000")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	// Step 2: Create a new grpc server
	s := grpc.NewServer()

	// Step 3: Register the service using grpc server
	greetpb.RegisterGreetServiceServer(s, &server{})

	// Step 4: Use the listener for the server
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
