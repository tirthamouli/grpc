# gRPC

## What is gRPC

1. It is a free and opensource framework developed by google
2. It is now a part of the Cloud Native Computation Foundation (CNCF) - Like Docker, etc
3. At a highlevel - It allows you to define REQUEST and RESPONSE for RPC(Remote Procedure calls) and handles all the rest

### On top of it

1. It is modern
2. Fast and efficient
3. Build on top of HTTP/2
4. Low latency
5. Supports streaming
6. Language Independent
7. Makes it easy to plug in authentication, load balancing, logging and monitoring

## What is RPC

1. RPC - Remote Procedure Call
2. In the CLIENT code, it looks like you are just calling a function directly on the SERVER

### It is not a new concept (CORBA had it before)

### At the core of gRPC we need to define the message and service using Protocol Buffers. The rest of the gPRC code will be generated for us and we need to implement the service. One .proto file will work for over 12 programming languages, and allows you to use a framework that scales to millions of RPC per second

## Why Protocol Buffer

1. Protocol Buffers are language agnostic
2. Code can be generated for pretty much any language
3. Data is binary and efficienly serialized (small payload)
4. Very convenient for transporting large amount of data
5. Protocol buffers allow for easy API evolution using rules

## Why should you learn it

1. Many companies have embraced it fully in production
   a. Google (internally and for google cloud services)
   b. Netfix
   c. Square
   d. CoreOS
   e. CockroachDB
2. gRPC is the future of microservices API and mobile-server API (and maybe even web APIs)

## Efficiency of protocol buffers over JSON

1. gRPC user protocol buffers for communication
2. JSON payload size is a lot larger than protocol buffer
3. Parsing JSON is a CPU intensive task

### Overall protocol buffers are faster and more efficient than JSON

1. Easy to write message definition
2. The definition of the API is independent of the implementions
3. A huge amount of boiler plate code for a variety of languages will be generated from a ten lines .proto file
4. Protocol Buffers define rules to make API evolve without breaking existing clients, which are helpful to microservices

### HTTP 1.1 vs HTTP 2

1. http 1.1 was created in 1997 and http 2 was created in 2015
2. http 1.1 can only send response to request whereas http 2 can also send server push

## 4 Types of gRPC

1. Unary - Classic request response: ALl our API calls (HTTP REST) with HTTP2 and Protocol buffer benifits
2. Server Streaming - Many response for one request
3. Client Streaming - Many request and one response
4. Bi-directional Streaming - Many request, many response

### Streaming is really helpful for big data or chat

## Scalability in gRPC

1. gRPC servers are asyncronous by default
2. This means that threads are not blocked on request
3. Can server millions of request in parellel
4. gRPC clients can be syncronous or async
5. gRPC clients can perform client side load balancing when there are multiple gPRC servers. Clients horizontal scaling
   Google make 10 Billions gRPC request per second

## Security in gRPC

1. It is a secure framework by default and strongly advocate you to use SSL in you API. That means gRPC has security as a first class citizen
2. Each language will provide an API to load gRPC with the required certificates and will provide encryption capabilty out of the box
3. Additionaly using Interceptors, we can provide authentication.

## gRPC vs REST

1. gPRC uses protocol buffer which is faster whereas REST uses JSON which is slower
2. gRPC uses http 2 and REST uses http or http 2
3. gRPC is bidirectional and async where as REST is client => server request only
4. gRPC is API oriented (what should be done) whereas REST is CRUD oriented (Resource oriented)
5. Code generation through protocol buffer first class citizen whereas in REST there is no code generation (some code generation can be done through open API and swagger but they are second class citizen as they are add ons)
6. gRPC does the plumbing for us where as for REST we need to do the plumbing i.e. controllers (not calling functions directly)
